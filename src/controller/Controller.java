package controller;

import java.io.File;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations()
	{
		manager.loadStations("." + File.separator + "data" + File.separator + "Divvy_Stations_2017_Q3Q4.csv" );
	}
	
	public static void loadTrips()
	{
		manager.loadTrips("." + File.separator + "data" + File.separator + "Divvy_Trips_2017_Q4.csv" );
	}
		
	public static DoublyLinkedList <VOTrip> getTripsOfGender (String gender)
	{
		return manager.getTripsOfGender(gender);
	}
	
	public static DoublyLinkedList <VOTrip> getTripsToStation (int stationID)
	{
		return manager.getTripsToStation(stationID);
	}
}
