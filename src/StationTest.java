import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.UnicamenteIdentificado;
import model.vo.VOTrip;


public class StationTest
{

	private Controller controlador;
	private ListaDoblementeEncadenada<UnicamenteIdentificado> listaDePrueba;

	@Before
	public void setUpEscenario1()
	{
		controlador = new Controller();
		listaDePrueba = new ListaDoblementeEncadenada<>();

		listaDePrueba.add( new VOTrip( 0, "1/2/2000", "1/2/2000", 123, 0, 1, "Estaci�n 1", 2, "Estaci�n 2", "General", "Male", 1900 ) );
		listaDePrueba.add( new VOTrip( 1, "1/2/2000", "1/2/2000", 456, 0, 2, "Estaci�n 2", 3, "Estaci�n 3", "General", "Female", 1995 ) );
		listaDePrueba.add( new VOTrip( 2, "1/2/2000", "1/2/2000", 789, 0, 3, "Estaci�n 3", 1, "Estaci�n 1", "General", "Male", 2000 ) );
	}
	
	@Before
	public void setUpEscenario2()
	{
		controlador = new Controller();
		listaDePrueba = new ListaDoblementeEncadenada<>();

		listaDePrueba.add( new VOTrip( 0, "1/2/2000", "1/2/2000", 123, 0, 1, "Estaci�n 1", 2, "Estaci�n 2", "General", "Male", 1900 ) );
		listaDePrueba.add( new VOTrip( 1, "1/2/2000", "1/2/2000", 456, 0, 2, "Estaci�n 2", 3, "Estaci�n 3", "General", "Female", 1995 ) );
		listaDePrueba.add( new VOTrip( 2, "1/2/2000", "1/2/2000", 789, 0, 3, "Estaci�n 3", 1, "Estaci�n 1", "General", "Male", 2000 ) );
		listaDePrueba.add( new VOTrip( 0, "1/2/2000", "1/2/2000", 123, 0, 1, "Estaci�n 1", 2, "Estaci�n 2", "General", "Male", 1900 ) );
		listaDePrueba.add( new VOTrip( 1, "1/2/2000", "1/2/2000", 456, 0, 2, "Estaci�n 2", 3, "Estaci�n 3", "General", "Female", 1995 ) );
		listaDePrueba.add( new VOTrip( 2, "1/2/2000", "1/2/2000", 789, 0, 3, "Estaci�n 3", 1, "Estaci�n 1", "General", "Male", 2000 ) );
		listaDePrueba.add( new VOTrip( 0, "1/2/2000", "1/2/2000", 123, 0, 1, "Estaci�n 1", 2, "Estaci�n 2", "General", "Male", 1900 ) );
		listaDePrueba.add( new VOTrip( 1, "1/2/2000", "1/2/2000", 456, 0, 2, "Estaci�n 2", 3, "Estaci�n 3", "General", "Female", 1995 ) );
		listaDePrueba.add( new VOTrip( 2, "1/2/2000", "1/2/2000", 789, 0, 3, "Estaci�n 3", 1, "Estaci�n 1", "General", "Male", 2000 ) );
		listaDePrueba.add( new VOTrip( 0, "1/2/2000", "1/2/2000", 123, 0, 1, "Estaci�n 1", 2, "Estaci�n 2", "General", "Male", 1900 ) );
	}

	@Test
	public void testAgregarAlInicio()
	{
		setUpEscenario1();

		try
		{
			VOTrip viaje = new VOTrip(3, "1/2/2000", "1/2/2000", 4488, 12, 3, "La Colmena", 12, "Express", "Preferencial", "Female", 1998);
			assertEquals( "Debe dar true", listaDePrueba.addFirst(viaje), true );
			assertEquals( "Deben ser iguales", listaDePrueba.get(0).darIdentificador(), String.valueOf(3) );
		}
		catch (Exception e)
		{
			fail("Mala la rata.");
		}
	}

	@Test
	public void testAgregarAlFinal()
	{
		setUpEscenario1();
		
		try
		{
			VOTrip viaje = new VOTrip(3, "1/2/2000", "1/2/2000", 4488, 12, 3, "La Colmena", 12, "Express", "Preferencial", "Female", 1998);
			assertEquals( "Debe dar true", listaDePrueba.add(viaje), true );
			assertEquals( "Deben ser iguales", listaDePrueba.get( listaDePrueba.size() - 1 ).darIdentificador(), String.valueOf(3) );
		}
		catch (Exception e)
		{
			fail("Mala la rata.");
		}
	}

	@Test
	public void testObtenerElemento()
	{
		setUpEscenario1();
		
		assertEquals( "Deben ser iguales", listaDePrueba.get(1).darIdentificador(), "1" );
	}

	@Test
	public void buscarElemento()
	{
		setUpEscenario1();
		
		assertEquals("Mala la rata", "2", listaDePrueba.get(2).darIdentificador() );
	}

	@Test
	public void eliminarElemento()
	{
		setUpEscenario1();
		// Eliminar uno de los del set up.
		
	}
	
	@Test
	public void agregarEnLaMitad()
	{
		setUpEscenario1();
		
		try
		{
			VOTrip viaje = new VOTrip(10, "1/2/2000", "1/2/2000", 4488, 12, 3, "La Colmena", 12, "Express", "Preferencial", "Female", 1998);
			listaDePrueba.add( 2 , viaje );
			
			assertEquals("Mala la rata", "10", listaDePrueba.get(2).darIdentificador() );
		}
		catch (Exception e)
		{
			fail("No deber�a lanzar excepci�n.");
		}
	}

	@Test
	public void comprobarTamanio()
	{
		setUpEscenario2();
		assertTrue("El tama�o no es correcto", listaDePrueba.getSize() == 10 );
	}

}
