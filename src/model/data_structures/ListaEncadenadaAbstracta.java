package model.data_structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;


/**
 * Clase que contiene lo com�n entre las listas encadenadas.
 * @param <E> Elemento que se almacenar� en la lista, el elemento debe ser �nicamente identificado.
 * Tomado de mi Nivel 9 de APO 2 Honores.
 * @author Christian Aparicio.
 * modificado por Juan Pablo Correa.
 */
public abstract class ListaEncadenadaAbstracta<E extends UnicamenteIdentificado> implements List<E>
{
	/**
	 * Atributo que indica la cantidad de elementos que han sido almacenados en la lista.
	 */
	protected int cantidadElementos;

	/**
	 * Primer nodo de la lista.
	 */
	protected NodoListaSencilla<E> primerNodo;

	/**
	 * M�todo que retorna los elementos de la lista en forma de un arreglo de objetos.
	 */
	public Object[] toArray() 
	{
		UnicamenteIdentificado[] arreglo = new UnicamenteIdentificado[size()];
		NodoListaSencilla<E> actual = primerNodo;
		int pos = 0;
		while(actual != null)
		{
			arreglo[pos] = actual.darElemento();
			actual = actual.darSiguiente();
			pos ++;
		}

		return arreglo;
	}

	/**
	 * M�todo que devuelve una lista de elementos de la lista como un arreglo de U.
	 * @param array El arreglo donde se deben guardar los elementos a menos que no quepan.
	 * @return Un arreglo con todos los elementos de la lista.
	 */
	public<U> U[] toArray(U[] array) 
	{
		if(array.length < size())
		{
			return (U[]) toArray();
		}
		else
		{
			NodoListaSencilla<E> actual = primerNodo;
			int pos =0;
			while(actual != null)
			{
				array[pos] = (U) actual.darElemento();
				actual = actual.darSiguiente();
				pos ++;
			}
			if(array.length > size())
			{
				array[size()] = null;
			}
			return array;
		}
	}


	/**
	 * Indica el tama�o de la lista.
	 * @return La cantidad de nodos de la lista.
	 */
	public int size() 
	{
		return cantidadElementos;
	}

	/**
	 * Reemplaza el elemento de la posici�n por el elemento que llega por par�metro.
	 * @param index La posici�n en la que se desea reemplazar el elemento.
	 * @param element El nuevo elemento que se quiere poner en esa posici�n
	 * @return el elemento que se ha retirado de esa posici�n.
	 * @throws IndexOutOfBoundsException si la posici�n es < 0 o la posici�n es >= size()
	 */
	public E set(int index, E element) throws IndexOutOfBoundsException 
	{
		// TODO Completar seg�n la documentaci�n
		if( index < 0 || index > size() )
		{
			throw new IndexOutOfBoundsException( "El �ndice no es v�lido, se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + cantidadElementos );
		}

		NodoListaSencilla<E> nodoActual = null;

		if( primerNodo != null )
		{
			nodoActual = primerNodo;

			while( index != 0 && nodoActual != null )
			{
				nodoActual = nodoActual.darSiguiente();
				index --;
			}

			nodoActual.cambiarElemento( element );

			return nodoActual.darElemento();
		}
		else
		{
			return null;
		}
	}

	/**
	 * Borra de la lista todos los elementos en la colecci�n que llega por par�metro
	 * @param coleccion la colecci�n de elmentos que se desea eliminar. coleccion != null
	 * @return true en caso que se elimine al menos un elemento o false en caso contrario
	 */
	public boolean removeAll(Collection<?> c) 
	{
		boolean modificado = false;
		for(Object objeto: c)
		{
			modificado |= remove(objeto);
		}
		return modificado;
	}

	/**
	 * Indica la  �ltima posici�n  donde aparece el objeto por par�metro en la lista
	 * @param objeto el objeto buscado en la lista. objeto != null
	 * @return la  �ltima posici�n del objeto en la lista o -1 en caso que no exista
	 */
	public int lastIndexOf(Object pObjeto)
	{
		// TODO Completar seg�n la documentaci�n
		int ultimaPosicionIgualAlObjeto = -1;
		int indiceGeneral = 0;

		if( !isEmpty() )
		{
			// Caso donde el primer nodo tiene el elemento por par�metro
			if( primerNodo.darElemento().equals( pObjeto ) )
			{
				ultimaPosicionIgualAlObjeto = indiceGeneral;
			}

			// Va buscando cu�les otros nodos tienen el elemento por par�metro
			NodoListaSencilla<E> actual = primerNodo;

			while( actual != null )
			{
				indiceGeneral++;

				// Valida si el nodo actual tiene al objeto
				if( actual.darElemento().equals(pObjeto) )
				{
					ultimaPosicionIgualAlObjeto = indiceGeneral;
				}

				actual = actual.darSiguiente();
			}
		}

		return ultimaPosicionIgualAlObjeto;
	}

	/**
	 * Devuelve un iterador sobre la lista
	 * El iterador empieza en el primer elemento
	 * @return un nuevo iterador sobre la lista
	 */
	public Iterator<E> iterator() 
	{
		return new IteradorSencillo<E>(primerNodo);
	}

	/**
	 * Indica si la lista est� vacia
	 * @return true si la lista est� vacia o false en caso contrario
	 */
	public boolean isEmpty() 
	{
		// TODO Completar seg�n la documentaci�n
		if( primerNodo == null )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Indica la posici�n en la lista del objeto que llega por par�metro
	 * @param objeto el objeto que se desea buscar en la lista. objeto != null
	 * @return la posici�n del objeto o -1 en caso que no se encuentre en la lista
	 */
	public int indexOf(Object o) 
	{
		// TODO Completar seg�n la documentaci�n
		int indiceGeneral = 0;
		int indiceDelObjeto = -1;

		if( !isEmpty() )
		{
			NodoListaSencilla<E> actual = primerNodo;

			while( actual != null )
			{
				indiceGeneral ++;

				if( actual.darSiguiente().darElemento().equals( o ) )
				{
					indiceDelObjeto = indiceGeneral + 1;
				}

				actual = actual.darSiguiente();
			}
		}

		return indiceDelObjeto;
	}

	/**
	 * Devuelve el elemento de la posici�n dada
	 * @param pos la posici�n  buscada
	 * @return el elemento en la posici�n dada
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public E get(int index) throws IndexOutOfBoundsException
	{
		// TODO Completar seg�n la documentaci�n
		E elemento = null;

		// Verifica que el �ndice s� sea v�lido
		if( index < 0 || index >= size() )
		{
			throw new IndexOutOfBoundsException( "El �ndice no es v�lido, se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + cantidadElementos );
		}

		// Caso donde se desea obtener el elemento en el primer nodo
		if( index == 0 && primerNodo != null )
		{
			elemento = primerNodo.darElemento();
		}
		// Donde el elemento que se desea obtener no est� en el primer nodo
		else
		{
			NodoListaSencilla<E> actual = primerNodo;

			for( int i = 0; i < index; i++ )
			{
				actual = actual.darSiguiente();
			}

			elemento = actual.darElemento();
		}

		return elemento;
	}

	/**
	 * Devuelve el nodo de la posici�n dada
	 * @param pos la posici�n  buscada
	 * @return el nodo en la posici�n dada 
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public NodoListaSencilla<E> darNodo(int index)
	{
		if(index < 0 || index > cantidadElementos)
		{
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + cantidadElementos);
		}

		NodoListaSencilla<E> actual = primerNodo;
		int posActual = 0;
		while(actual != null && posActual < index)
		{
			actual = actual.darSiguiente();
			posActual ++;
		}

		return actual;
	}

	/**
	 * Indica si la lista contiene todos los objetos de la colecci�n dada
	 * @param coleccion la colecci�n de objetos que se desea buscar. coleccion !=null
	 * @return true en caso que todos los objetos est�n en la lista o false en caso contrario
	 */
	public boolean containsAll(Collection<?> c) 
	{
		boolean contiene = true;
		for(Object objeto: c)
		{
			contiene &= contains(objeto);
		}
		return contiene;
	}

	/**
	 * Indica si la lista contiene el objeto indicado
	 * @param objeto el objeto que se desea buscar en la lista. objeto != null
	 * @return true en caso que el objeto est� en la lista o false en caso contrario
	 */
	public boolean contains( Object pObjeto ) 
	{
		// TODO Completar seg�n la documentaci�n
		boolean contiene = false;

		if( !isEmpty() )
		{
			NodoListaSencilla<E> actual = primerNodo;
			while( actual != null && !contiene )
			{
				if( actual.darElemento().equals( pObjeto ) )
				{
					contiene = true;
				}

				actual = actual.darSiguiente();
			}
		}

		return contiene;
	}

	/**
	 * Borra todos los elementos de la lista. Actualiza la cantidad de elementos en 0
	 * <b>post:</b> No hay elementos en la lista
	 */
	public void clear() 
	{
		// TODO Completar seg�n la documentaci�n
		primerNodo.cambiarSiguiente( null );
		primerNodo = null;
		cantidadElementos = 0;
	}

	/**
	 * Agrega todos los elementos de la colecci�n a la lista a partir de la posici�n indicada
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param pos la posici�n a partir de la cual se desean agregar los elementos
	 * @param coleccion la colecci�n de elementos que se desea agregar
	 * @return true si al menos uno de los elementos se agrega o false en caso contrario
	 * @throws NullPointerException Si alguno de los elementos que se quiere agregar es null
	 * @throws IndexOutOfBoundsException si indice < 0 o indice > size()
	 */
	public boolean addAll(int pos, Collection<? extends E> c) 
	{
		int size = cantidadElementos;
		for(E actual: c)
		{
			add(actual);
			pos++;
		}

		return cantidadElementos > size;
	}

	/**
	 * Agrega a la lista todos los elementos de la colecci�n
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param coleccion la colecci�n  de elementos que se desea agregar
	 * @return true en caso que se agregue al menos un elemento false en caso contrario
	 * @throws NullPointerException si alguno de los elementos que se desea agregar es null
	 */
	public boolean addAll(Collection<? extends E> c) 
	{
		boolean modificado = false;
		for(E actual: c)
		{
			modificado |= add(actual);
		}
		return modificado;
	}

}
