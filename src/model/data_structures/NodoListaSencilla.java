package model.data_structures;

import java.io.Serializable;

/**
 * Representa un nodo de la lista sencilla.
 * @param <E> Elemento que se almacenará en la lista, el elemento debe ser únicamente identificado.
 * Tomado de mi Nivel 9 de APO 2 Honores.
 * @author Christian Aparicio.
 * modificado por Juan Pablo Correa.
 */
public class NodoListaSencilla<E extends UnicamenteIdentificado>
{
	/**
	 * Elemento almacenado en el nodo.
	 */
	protected E elemento;
	
	/**
	 * Siguiente nodo.
	 */
	//TODO Defina el atributo siguiente como el siguiente nodo de la lista.
	protected NodoListaSencilla<E> siguiente;
	
	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenará en el nodo. elemento != null
	 */
	public NodoListaSencilla(E pElemento)
	{
		//TODO Completar de acuerdo a la documentación.
		elemento = pElemento;
	}
	
	/**
	 * Método que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(NodoListaSencilla<E> pSiguiente)
	{
		//TODO Completar de acuerdo a la documentación.
		siguiente = pSiguiente;
	}
	
	/**
	 * Método que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public E darElemento()
	{
		//TODO Completar de acuerdo a la documentación.
		return elemento;
	}
	
	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenará en el nodo.
	 */
	public void cambiarElemento(E pElemento)
	{
		//TODO Completar de acuerdo a la documentación.
		elemento = pElemento;
	}
	
	/**
	 * Método que retorna el identificador del nodo.
	 * Este identificador es el identificador del elemento que almacena.
	 * @return Identificador del nodo (identificador del elemento almacenado).
	 */
	public String darIdentificador()
	{
		//TODO Completar de acuerdo a la documentación.
		return elemento.darIdentificador();
	}
	
	/**
	 * Método que retorna el siguiente nodo.
	 * @return Siguiente nodo
	 */
	public NodoListaSencilla<E> darSiguiente()
	{
		//TODO Completar de acuerdo a la documentación.
		return siguiente;
	}

}
