package model.data_structures;

/**
 * Representa un nodo de la lista doble.
 * @param <E> Elemento que se almacenará en la lista, el elemento debe ser únicamente identificado.
 * Tomado de mi Nivel 9 de APO 2 Honores.
 * @author Christian Aparicio.
 * modificado por Juan Pablo Correa.
 */

public class NodoListaDoble<E extends UnicamenteIdentificado> extends NodoListaSencilla<E>
{
	/**
	 * Constante de serialización
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Nodo anterior.
	 */
	//TODO Defina el atributo anterior como el nodo anterior de la lista.
	protected NodoListaDoble<E> anterior;
	
	/**
	 * Método constructor del nodo doblemente encadenado
	 * @param elemento elemento que se almacenará en el nodo.
	 */
	public NodoListaDoble(E elemento) 
	{
		//TODO Completar de acuerdo a la documentación.
		super( elemento );
		anterior = null;
	}
	
	/**
	 * Método que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public NodoListaDoble<E> darAnterior()
	{
		//TODO Completar de acuerdo a la documentación.
		return anterior;
	}
	
	/**
	 * Método que cambia el nodo anterior por el que llega como parámetro.
	 * @param anterior Nuevo nodo anterior.
	 */
	public void cambiarAnterior(NodoListaDoble<E> pAnterior)
	{
		//TODO Completar de acuerdo a la documentación.
		anterior = pAnterior;
	}
}
