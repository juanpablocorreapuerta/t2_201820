package model.data_structures;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Clase que representa la lista doblemente encadenada
 * @param <E> Tipo del objeto que almacena el iterador de la lista.
 * Tomado de mi Nivel 9 de APO 2 Honores.
 * @author Christian Aparicio.
 * modificado por Juan Pablo Correa.
 */

public class ListaDoblementeEncadenada<E extends UnicamenteIdentificado> extends ListaEncadenadaAbstracta<E> implements DoublyLinkedList<E> 
{

	/**
	 * Construye una lista vacia
	 * <b>post:< /b> se ha inicializado el primer nodo en null
	 */
	public ListaDoblementeEncadenada() 
	{
		primerNodo = null;
		cantidadElementos = 0;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaDoblementeEncadenada(E nPrimero)
	{
		if(primerNodo == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		primerNodo = new NodoListaDoble<E>(nPrimero);
		cantidadElementos = 1;
	}

	/**
	 * Agrega un elemento al final de la lista
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
	 * Se actualiza la cantidad de elementos.
	 * @param e el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean add(E elemento ) 
	{
		// TODO Completar seg�n la documentaci�n
		boolean agrego = false;
		if( elemento == null )
		{
			throw new NullPointerException( "El elemento que intenta a�adir es nulo" );
		}

		// Crea el nuevo nodo con el elemento dado por par�metro
		NodoListaDoble<E> nuevoNodo = new NodoListaDoble<E>(elemento);

		if( isEmpty() )
		{
			primerNodo = nuevoNodo;
			cantidadElementos++;
			agrego = true;
		}
		else
		{
			NodoListaDoble<E> ultimoNodo = (NodoListaDoble<E>) darNodo( size() - 1 );
			ultimoNodo.cambiarSiguiente( nuevoNodo );
			nuevoNodo.cambiarAnterior( ultimoNodo );
			cantidadElementos++;
			agrego = true;
		}

		return agrego;
	}

	/**
	 * Agrega un elemento al final de la lista. Actualiza la cantidad de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param elemento el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public void add(int indice, E elemento) 
	{
		// TODO Completar seg�n la documentaci�n
		if( indice > size() || indice < 0 )
		{
			throw new IndexOutOfBoundsException( "Est� pidiendo el indice: " + indice + " y el tama�o de la lista es de " + cantidadElementos );
		}
		if( elemento == null )
		{
			throw new NullPointerException( "El elemento que desea agregar es nulo" );
		}

		NodoListaDoble<E> nuevoNodo = new NodoListaDoble<E>( elemento );
		NodoListaDoble<E> actual = (NodoListaDoble<E>) darNodo( indice );
		NodoListaDoble<E> anterior =  actual.darAnterior();
		NodoListaDoble<E> siguiente = (NodoListaDoble<E>)actual.darSiguiente();

		if( anterior != null )
		{
			anterior.cambiarSiguiente( nuevoNodo );
			if( siguiente != null )
			{
				siguiente.cambiarAnterior( nuevoNodo );
			}

			nuevoNodo.cambiarSiguiente( siguiente );
			nuevoNodo.cambiarAnterior( anterior );
			cantidadElementos++;
		}
	}

	/**
	 * M�todo que retorna el iterador de la lista.
	 * @return el iterador de la lista.
	 */
	public ListIterator<E> listIterator() 
	{
		return new IteradorLista<E>((NodoListaDoble<E>) primerNodo);
	}

	/**
	 * M�todo que retorna el iterador de la lista desde donde se indica.
	 * @param index �ndice desde se quiere comenzar a iterar.
	 * @return el iterador de la lista.
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public ListIterator<E> listIterator(int index) 
	{
		if(index< 0 || index >= size())
			throw new IndexOutOfBoundsException("El �ndice buscado est� por fuera de la lista.");
		return new IteradorLista<E>((NodoListaDoble<E>) darNodo(index));
	}

	/**
	 * Elimina el nodo que contiene al objeto que llega por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param objeto el objeto que se desea eliminar. objeto != null
	 * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
	 */
	public boolean remove(Object objeto ) 
	{
		// TODO Completar seg�n la documentaci�n
		if( isEmpty() )
		{
			return false;
		}
		else
		{
			// Recorrido parcial almacenando el nodo actual
			NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;

			while( actual != null && !actual.darElemento().equals( objeto ) )
			{
				actual = (NodoListaDoble<E>) actual.darSiguiente();
			}

			// Eval�a si se encontr� el objeto en la lista
			if( actual == null ) 
			{
				return false;
			}
			else
			{
				NodoListaDoble<E> anterior = actual.darAnterior();
				NodoListaDoble<E> siguiente = (NodoListaDoble<E>) actual.darSiguiente();

				if( anterior != null)
				{
					anterior.cambiarSiguiente( actual.darSiguiente() );
				}
				if( siguiente != null )
				{
					siguiente.cambiarAnterior( actual.darAnterior() );
				}

				cantidadElementos--;
				return true;
			}
		}
	}

	/**
	 * Elimina el nodo en la posici�n por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param posicion la posici�n que se desea eliminar
	 * @return el elemento eliminado
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public E remove(int posicion) 
	{
		// TODO Completar seg�n la documentaci�n
		if( posicion < 0 || posicion >= size() )
		{
			throw new IndexOutOfBoundsException( "El �ndice no es v�lido, se est� pidiendo la posici�n: "
					+ posicion + " y el tama�o de la lista es de " + cantidadElementos );
		}

		E removido = null;

		if( !isEmpty() )
		{
			// Se eval�a si el nodo a remover es el primero o no
			if( posicion == 0 ) 
			{
				removido = primerNodo.darElemento();
				primerNodo = primerNodo.darSiguiente();
				cantidadElementos--;
			}
			else
			{
				NodoListaDoble<E> anterior = (NodoListaDoble<E>) darNodo( posicion - 1 );
				NodoListaDoble<E> actual = (NodoListaDoble<E>) anterior.darSiguiente();
				removido = actual.darElemento();
				NodoListaDoble<E> siguiente = (NodoListaDoble<E>) actual.darSiguiente();
				// Como el nodo no es el primero, se sabe que va a tener un anterior
				anterior.cambiarSiguiente( actual.darSiguiente() );
				if( siguiente != null )
				{
					siguiente.cambiarAnterior( anterior );
				}
				cantidadElementos--;
			}
		}

		return removido;
	}

	/**
	 * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro.
	 * Actualiza la cantidad de elementos
	 * @param coleccion la colecci�n de elementos a mantener. coleccion != null
	 * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
	 */
	@SuppressWarnings("unchecked")
	public boolean retainAll(Collection<?> coleccion) 
	{
		// TODO Completar seg�n la documentaci�n
		boolean modifico = false;

		if( !isEmpty() )
		{
			clear();
			modifico = true;
		}
		
		Object[] nuevaColeccion = coleccion.toArray();

		primerNodo = new NodoListaDoble<E>((E)nuevaColeccion[0]);

		for( int i = 1; i < nuevaColeccion.length && modifico; i++ )
		{
			modifico = add( (E)nuevaColeccion[i] );
			
		}

		return modifico;
	}

	public boolean addFirst( E elemento )
	{
		if( elemento == null )
			throw new NullPointerException( "El elemento es nulo" );
		
		NodoListaDoble<E> actual = (NodoListaDoble<E>)primerNodo;
		primerNodo = new NodoListaDoble<E>(elemento);
		primerNodo.cambiarSiguiente( actual );
		cantidadElementos++;
		return true;
	}
	

	@Override
	public List<E> subList(int fromIndex, int toIndex)
	{
		return null;
	}
	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return cantidadElementos;
	}

}
