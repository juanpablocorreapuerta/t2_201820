package model.data_structures;

import java.io.Serializable;
import java.util.Iterator;

/**
 * Clase que representa el iterador sencillo (s�lo avanza hacia adelante).
 * @param <E> Tipo del objeto que almacena el iterador de la lista.
 * Tomado de mi Nivel 9 de APO 2 Honores.
 * @author Christian Aparicio.
 * modificado por Juan Pablo Correa.
 */

public class IteradorSencillo<E extends UnicamenteIdentificado> implements Iterator<E>, Serializable 
{

	/**
	 * Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * El nodo donde se encuentra el iterador.
	 */
	private NodoListaSencilla<E> actual;

	
	public IteradorSencillo(NodoListaSencilla<E> primerNodo) 
	{
		actual = primerNodo;
	}
	
	/**
     * Indica si a�n hay elementos por recorrer
     * @return true en caso de que  a�n haya elemetos o false en caso contrario
     */
	public boolean hasNext() 
	{
		return actual != null;
	}

	/**
     * Devuelve el siguiente elemento a recorrer
     * <b>post:</b> se actualizado actual al siguiente del actual
     * @return objeto en actual
     */
	public E next() 
	{
		E valor = actual.darElemento();
		actual = actual.darSiguiente();
		return valor;
	}

}
