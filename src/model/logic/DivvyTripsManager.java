package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.*;
import model.data_structures.*;

public class DivvyTripsManager implements IDivvyTripsManager
{

	/**
	 * Lista para almacenar las estaciones.
	 */
	private ListaDoblementeEncadenada<UnicamenteIdentificado> listaEstaciones;

	/**
	 * Lista para almacenar los viajes.
	 */
	private ListaDoblementeEncadenada<UnicamenteIdentificado> listaViajes;

	/**
	 * M�todo que carga las estaciones a la lista de estaciones mediante la ruta del archivo.
	 * @param stationsFile ruta con el archivo de estaciones. 	 
	 */
	public void loadStations( String stationsFile )
	{
		listaEstaciones = new ListaDoblementeEncadenada<>();

		// System.out.println("A load Stations lleg� la ruta " + stationsFile );
		try
		{
			BufferedReader reader = new BufferedReader( new FileReader( new File( stationsFile ) ) );

			String linea = reader.readLine();

			// Lee una l�nea para omitir los encabezados
			linea = reader.readLine();

			while( linea != null )
			{				
				String[] lineaSeparada = linea.split(",");

				Station nuevaEstacion = new Station( Integer.parseInt( lineaSeparada[0] ), lineaSeparada[1], lineaSeparada[2], Double.parseDouble( lineaSeparada[3] ), Double.parseDouble( lineaSeparada[4] ), Integer.parseInt( lineaSeparada[5] ), lineaSeparada[6] );

				listaEstaciones.addFirst( nuevaEstacion );

				linea = reader.readLine();
			}

			reader.close();
		}
		catch(Exception e)
		{
			System.out.println("Error por " + e.getMessage());
		}
	}

	/**
	 * M�todo que carga los viajes a la lista de viajes mediante la ruta del archivo.
	 * @param tripsFile ruta con el archivo de viajes. 	 
	 */
	public void loadTrips (String tripsFile)
	{
		try
		{
			listaViajes = new ListaDoblementeEncadenada<>();

			BufferedReader reader = new BufferedReader( new FileReader( new File( tripsFile ) ) );

			String linea = reader.readLine();
			linea = reader.readLine();


			while( linea != null )
			{
				String[] lineaSeparada = linea.split(",");

				// Para completar las l�neas que no tienen g�nero ni a�o de nacimiento
				String genero = "Male";
				int anioNacimiento = 1990;

				if( lineaSeparada.length == 12 )
				{
					genero = lineaSeparada[10];
					anioNacimiento = Integer.parseInt( lineaSeparada[11] );
				}

				// Se crea el viaje y se a�adea la lista.
				VOTrip nuevoViaje = new VOTrip(Integer.parseInt(lineaSeparada[0]), lineaSeparada[1], lineaSeparada[2], Integer.parseInt( lineaSeparada[3] ), Integer.parseInt( lineaSeparada[4] ), Integer.parseInt( lineaSeparada[5] ), lineaSeparada[6], Integer.parseInt( lineaSeparada[7] ), lineaSeparada[8], lineaSeparada[9], genero, anioNacimiento );
				//System.out.println(linea);
				listaViajes.addFirst( nuevoViaje );

				linea = reader.readLine();
			}

			reader.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	@Override
	public DoublyLinkedList<VOTrip> getTripsOfGender (String gender)
	{
		// TODO Auto-generated method stub
		ListaDoblementeEncadenada<VOTrip> lista = new ListaDoblementeEncadenada<VOTrip>();

		Iterator<UnicamenteIdentificado> iterador = listaViajes.iterator();

		while( iterador.hasNext() )
		{
			VOTrip elementoActual = (VOTrip) iterador.next();
			if( elementoActual.darGenero().equalsIgnoreCase(gender) )
			{
				lista.addFirst(elementoActual);
			}
		}

		return lista;
	}


	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID)
	{
		ListaDoblementeEncadenada<VOTrip> lista = new ListaDoblementeEncadenada<VOTrip>();

		Iterator<UnicamenteIdentificado> iterador = listaViajes.iterator();

		while( iterador.hasNext() )
		{
			VOTrip viajeActual = (VOTrip) iterador.next();

			if( viajeActual.darIdEstacionDestino() == stationID )
			{
				lista.addFirst( viajeActual );
			}
		}

		return lista;
	}	

}
