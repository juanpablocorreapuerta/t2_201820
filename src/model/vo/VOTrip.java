package model.vo;

import java.util.Calendar;

import model.data_structures.UnicamenteIdentificado;

public class VOTrip implements UnicamenteIdentificado
{
	
	/**
	 * ID del viaje.
	 */
	private int id;
	
	/**
	 * Representa el tiempo de inicio.
	 */
	private String tiempoInicio;
	
	private String tiempoFinal;
	
	private int idBicileta;
	
	private int duracion;
	
	private int idEstacionSalida;
	
	private String nombreEstacionSalida;
	
	private int idEstacionDestino;
	
	private String nombreEstacionDestino;
	
	private String tipoUsuario;

	private String genero;
	
	private int anioNacimiento;
	
	
	public VOTrip( int id, String tiempoInicio, String tiempoFinal, int idBicicleta, int duracion, int idEstacionSalida, String nombreEstacionSalida, int idEstacionDestino, String nombreEstacionDestino, String tipoUsuario, String genero, int anioNacimiento )
	{
		this.id = id;
		this.tiempoInicio = tiempoInicio;
		this.tiempoFinal = tiempoFinal;
		this.idBicileta = idBicicleta;
		this.duracion = duracion;
		this.idEstacionSalida = idEstacionSalida;
		this.nombreEstacionDestino = nombreEstacionDestino;
		this.idEstacionDestino = idEstacionDestino;
		this.nombreEstacionDestino = nombreEstacionDestino;
		this.tipoUsuario = tipoUsuario;
		this.genero = genero;
		this.anioNacimiento = anioNacimiento;
	}

	public String darIdentificador()
	{
		return String.valueOf( id );
	}

	public String darTiempoInicio()
	{
		return tiempoInicio;
	}

	public String darTiempoFinal() {
		return tiempoFinal;
	}

	public int darIdBicileta()
	{
		return idBicileta;
	}

	public int darDuracion()
	{
		return duracion;
	}

	public int darIdEstacionSalida()
	{
		return idEstacionSalida;
	}
	
	public String darNombreEstacionSalida()
	{
		return nombreEstacionSalida;
	}

	public int darIdEstacionDestino()
	{
		return idEstacionDestino;
	}
	
	public String darNombreEstacionDestino()
	{
		return nombreEstacionDestino;
	}
	
	public String darTipoUsuario()
	{
		return tipoUsuario;
	}
	
	public String darGenero()
	{
		return genero;
	}
	
	public int darAnioNacimiento()
	{
		return anioNacimiento;
	}
}
