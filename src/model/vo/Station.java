package model.vo;

import java.util.Calendar;
import java.util.Date;

import model.data_structures.UnicamenteIdentificado;

/**
 * Clase que representa una estaci�n.
 * @author Juan Pablo
 */
public class Station implements UnicamenteIdentificado
{
	/**
	 * Representa el ID de la estaci�n.
	 */
	private int id;

	/**
	 * Representa el nombre de la estaci�n.
	 */
	private String nombre;

	/**
	 * Representa la ciudad de la estaci�n.
	 */
	private String ciudad;

	/**
	 * Representa la latitud de la estaci�n.
	 */
	private double latitud;

	/**
	 * Representa la longitud de la estaci�n.
	 */
	private double longitud;

	/**
	 * Representa la capacidad de la estaci�n.
	 */
	private int capacidad;

	/**
	 * Representa el tiempo en l�nea de la estaci�n.
	 */
	private String fechaEnLinea;

	//----------------------
	//		Constructor
	//----------------------
	/**
	 * Construye una nueva estaci�n
	 * @param id
	 * @param nombre
	 * @param latitud
	 * @param longitud
	 * @param capacidad
	 * @param tiempoEnLinea
	 */
	public Station( int id, String nombre, String ciudad, double latitud, double longitud, int capacidad, String tiempoEnLinea )
	{
		this.id = id;
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.latitud = latitud;
		this.longitud = longitud;
		this.capacidad = capacidad;
		this.fechaEnLinea = tiempoEnLinea;
	}

	//----------------------
	//		M�todos
	//----------------------
	/**
	 * Retorna el id de la estaci�n.
	 * @return Id de la estaci�n.
	 */
	@Override
	public String darIdentificador() {
		// TODO Auto-generated method stub
		return String.valueOf(id);
	}

	/**
	 * Retorna el id de la estaci�n.
	 * @return id de la estaci�n.
	 */
	public String darNombre()
	{
		return nombre;
	}

	/**
	 * Retorna la latitud de la estaci�n.
	 * @return latitud de la estaci�n.
	 */
	public double darLatitud()
	{
		return latitud;
	}

	/**
	 * Retorna la longitud de la estaci�n.
	 * @return longitud de la estaci�n.
	 */
	public double darLongitud()
	{
		return longitud;
	}

	/**
	 * Retorna la capacidad de la estaci�n.
	 * @return capacidad de la estaci�n.
	 */
	public int darCapacidad()
	{
		return capacidad;
	}

	/**
	 * Retorna el tiempo en linea de la estaci�n.
	 * @return tiempo en linea de la estaci�n.
	 */
	public String darTiempoEnLinea()
	{
		return fechaEnLinea;
	}


}
